import React, { useEffect } from "react";
import Airtable from 'airtable';
import TestC from "./components/TestC";

const base = new Airtable({ apiKey: "key70l2I9iE1rMpX6" }).base('appoZeVeVqDiOkLUV');

function App(){
    const [test, setTest] = useState([])
    const [updates, setUpdates] = useState([])
    
    useEffect(() => {
        base("test")
        .select({ view: "Grid view"})
        .eachPage((records, fetchNextPage) => {
            setTest(records);
            console.log(records);
            fetchNextPage();
        });
        base("updates")
        .select({ view: "Grid view"})
        .eachPage((records, fetchNextPage) => {
            setUpdates(records);
            console.log(records);
            fetchNextPage();
        });
    }, []); 
    return (
        <>
         <h1>My Test</h1>
         {test.map(test => (
             <Test
             key={test.id}
             test={test}
             updates={updates.filter(update => update.fields.testid[0] = test.id)}
        />
         ))}
     </>
    );
}

export default App;