<?php 
    header('Content-Type: application/rss+xml');
    include "php/db_connection.php";
    
    $articles = $bdd->query('SELECT * FROM artciles ORDER BY date_time_post DESC LIMIT 0,25');
?>

<?php echo '<?xml version="1.0" encoding="UTF-8"?>';?>

<rss version="2.0">
    <channel>
        <title>Flux RSS my_veille</title>
        <description>my_veille flux RSS 2.0</description>
<?php
    for ($i=0; $i < sizeof($veilleRss); $i++) { 
?>
        <item>
            <image><?php echo $veilleRss[$i]['image'] ?></image>
            <title><?php echo ($veilleRss[$i]["topic"]); ?></title>
            <pubDate><?php echo $veilleRss[$i]["date"] ?></pubDate>
            <description><?php echo $veilleRss[$i]["synthesis"] ?></description>
            <description><?php echo $veilleRss[$i]["comment"] ?></description>
            <link><?php echo $veilleRss[$i]["link"] ?></link>
        </item>
<?php
        }
?>
    </channel>
</rss>